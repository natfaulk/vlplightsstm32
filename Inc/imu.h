#ifndef IMU_H
#define IMU_H

#include "stm32f1xx_hal.h"

#include <stdbool.h>

// make sure in contiguous memory
typedef struct
{
  float x;
  float y;
  float z;
} ImuData;

#define MPU9250_ADDRESS_AD0_LOW  0x68
#define MPU9250_ADDRESS_AD0_HIGH 0x69

#define ACC_FULL_SCALE_2_G       0x00
#define ACC_FULL_SCALE_4_G       0x08
#define ACC_FULL_SCALE_8_G       0x10
#define ACC_FULL_SCALE_16_G      0x18

#define GYRO_FULL_SCALE_250_DPS  0x00
#define GYRO_FULL_SCALE_500_DPS  0x08
#define GYRO_FULL_SCALE_1000_DPS 0x10
#define GYRO_FULL_SCALE_2000_DPS 0x18

#define MAG_MODE_POWERDOWN        0x0
#define MAG_MODE_SINGLE           0x1
#define MAG_MODE_CONTINUOUS_8HZ   0x2
#define MAG_MODE_EXTERNAL         0x4
#define MAG_MODE_CONTINUOUS_100HZ 0x6
#define MAG_MODE_SELFTEST         0x8
#define MAG_MODE_FUSEROM          0xF

void IMU_Init(I2C_HandleTypeDef *_hi2c);
bool IMU_BeginAccel(void);
bool IMU_BeginGyro(void);
bool IMU_BeginMag(uint8_t mode);
bool IMU_UpdateAccel(void);
bool IMU_UpdateGyro(void);
bool IMU_UpdateMag(void);

ImuData* IMU_GetAccel(void);
ImuData* IMU_GetGyro(void);
ImuData* IMU_GetMag(void);

#endif
