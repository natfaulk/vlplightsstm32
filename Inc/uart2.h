#ifndef UART2_H
#define UART2_H

#include "stm32f1xx_hal.h"

void UART2_Init(UART_HandleTypeDef *_huart);
void UART2_SendStr(const char * str);
void UART2_SendUint32(uint32_t i);
void UART2_SendDouble(double f);

void UART2_PutChar(char c);
uint16_t UART2_CharsAvailable(void);
uint8_t UART2_GetChar(void);

#endif /* end of include guard: UART_H */
