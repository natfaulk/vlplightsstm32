#ifndef UART_H
#define UART_H

#include "stm32f1xx_hal.h"

void UART_Init(UART_HandleTypeDef *_huart);
void UART_SendStr(const char * str);
void UART_SendUint32(uint32_t i);
void UART_SendDouble(double f);

void UART_PutChar(char c);
uint16_t UART_CharsAvailable(void);
uint8_t UART_GetChar(void);

#endif /* end of include guard: UART_H */
