#ifndef SERVER_HPP
#define SERVER_HPP

void SERVER_Init(void);
void SERVER_Tick(void);
bool SERVER_sendIp(const char* _hostname);
void SERVER_SendUDP(const char* data);
void SERVER_SendWS(const char* data);

#endif