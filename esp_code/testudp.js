const WebSocket = require('ws')
const LocalIP = require('my-local-ip')
request = require('request')

const wss = new WebSocket.Server({ port: 8000 })

wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    console.log('received: %s', message)
  })

  ws.send('something')
  console.log('client connected')
})

request.post({
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  },
  uri: 'http://vlp.natfaulk.com/set_pc_ip',
  body: `ip=${LocalIP()}`,
  method: 'POST'
}, (error, response, body) => {
  console.log('error:', error)
  console.log('statusCode:', response && response.statusCode)
  console.log('body:', body)
})
