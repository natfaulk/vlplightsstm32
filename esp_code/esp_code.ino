#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPClient.h>
#include <WiFiUDP.h>
#include <WebSocketsClient.h>

#include "debug.hpp"
#include "server.hpp"

const uint32_t  BAUD_RATE                               = 460800;
const char*     SSID                                    = "VlpNetwork";
const char*     PASSWORD                                = "hiddenpassword";
const uint32_t  WIFI_CONNECT_TIMEOUT                    = 300000;
const char*     BOARD_ID                                = "VLP_REC3";

const uint32_t REC_BUFF_LEN = 25000;
char recBuff[REC_BUFF_LEN];
uint32_t recBuff_i = 0;

void setup(void)
{
  DBG_Setup(BAUD_RATE);
  DBG_s("Setup started");

#if DEBUG_ENABLE == 0
  Serial.begin(BAUD_RATE);
#endif

  char hostname[20];
  snprintf(hostname, 20, BOARD_ID);
  WiFi.hostname(hostname);
  DBG_pair("Hostname set to ", hostname);

  WiFi.begin(SSID, PASSWORD);
  DBG_s("Connecting to ", false);DBG_s(SSID);

  uint32_t timeout = millis();
  while ((WiFi.status() != WL_CONNECTED) && (millis() - timeout < WIFI_CONNECT_TIMEOUT))
  {
    delay(500);
    DBG_s(".", false);
  }
  DBG_nl();

  if (WiFi.status() == WL_CONNECTED)
  {
    DBG_s("Connection successful");  
    DBG_pair("IP Address", WiFi.localIP().toString().c_str());

    if (MDNS.begin(hostname))
    {
      MDNS.addService("http", "tcp", 80);
      DBG_s("MDNS started");
    }
    else DBG_s("MDNS failed to start");

    DBG_s("Sending IP address to central server");
    if (SERVER_sendIp(hostname)) DBG_s("IP address successfully sent");
    else DBG_s("IP address failed to send");

    SERVER_Init();
    DBG_s("HTTP server started");
  }
  else
  {
    DBG_s("WiFi connection failed");
  }

  DBG_s("Setup finished");
}

void loop(void)
{
  SERVER_Tick();
  
  char temp;
  while (Serial.available() > 0)
  {
    temp = Serial.read();
    
    if (temp != '\n')
    {
      recBuff[recBuff_i++] = temp;
    }
    else
    {
      recBuff[recBuff_i++] = '\n';
      recBuff[recBuff_i] = '\0';
      // DBG_s(recBuff);
      SERVER_SendWS(recBuff);

      recBuff_i = 0;
    }

    // lose data rather than overflow the buffer
    if (recBuff_i >= REC_BUFF_LEN - 1) recBuff_i = 0;
  }
}

