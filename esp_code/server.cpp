#include "server.hpp"
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <WiFiUDP.h>
#include <WebSocketsClient.h>
#include "version.hpp"

#define SERVER_DEBUG_ENABLE 1

#if SERVER_DEBUG_ENABLE
#include "debug.hpp"
#endif

const uint32_t LOCAL_UDP_PORT = 4000;
const uint32_t REMOTE_UDP_PORT = 4001;

static ESP8266WebServer server(80);
static WiFiUDP UDP;

static IPAddress remoteIP;
static bool validRemoteIp = false;

void rootHandler(void);
void notfoundHandler(void);
void getversionHandler(void);
void setremoteipHandler(void);
void getStartWsHandler(void);

bool getPcIp(void);
void startWs(void);

void handleUDPServer();

const char WS_PATH[] = "/";
const uint32_t WS_PORT = 8000;
bool ws_connected = false;
  
WebSocketsClient webSocket;

void webSocketEvent(WStype_t type, uint8_t * payload, size_t length) {
	switch(type) {
		case WStype_DISCONNECTED:
    #if SERVER_DEBUG_ENABLE
			DBG_s("[WSc] Disconnected!");
    #endif
      ws_connected = false;
			break;
		case WStype_CONNECTED: {
    #if SERVER_DEBUG_ENABLE
			DBG_pair("[WSc] Connected to url", (char *)payload);
    #endif
			// send message to server when Connected
			webSocket.sendTXT("ESP Connected");
      ws_connected = true;
		}
			break;
		case WStype_TEXT:
    #if SERVER_DEBUG_ENABLE
			DBG_pair("[WSc] get text", (char *)payload);
    #endif
			// send message to server
			// webSocket.sendTXT("message here");
			break;
		case WStype_BIN:
    #if SERVER_DEBUG_ENABLE
			DBG_pair("[WSc] get binary length", (uint32_t)length);
    #endif
			// hexdump(payload, length);
			break;
	}
}

void SERVER_Init(void)
{
  server.on("/", rootHandler);
  server.on("/getversion", getversionHandler);
  server.on("/setremoteip", setremoteipHandler);
  server.on("/startWs", getStartWsHandler);
  server.onNotFound(notfoundHandler);
  server.begin();

#if SERVER_DEBUG_ENABLE
  DBG_s("UDP Setup started");
#endif

  uint32_t result = UDP.begin(LOCAL_UDP_PORT);

#if SERVER_DEBUG_ENABLE
  if (result == 1) DBG_s("UDP server sucessfully started");
  else DBG_s("UDP server failed to start");
  
  if (getPcIp())
  {
    DBG_s("Successfully got remote IP");
    DBG_pair("Remote IP", remoteIP.toString());
  }
  else DBG_s("Failed to get remote IP");
#endif

  startWs();
  
  // UDP.beginPacket("192.168.0.119", 4001);
  // UDP.write("Hello UDP!");
  // UDP.endPacket();
}

bool SERVER_sendIp(const char* _hostname)
{
  bool returnVal = false;
  HTTPClient http;
  http.begin("http://vlp.natfaulk.com/set_ip2");
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");
  String ip = String("ip=");
  ip += WiFi.localIP().toString();
  ip += "&hostname=";
  ip += _hostname;
  ip += "&hwid=";
  ip += ESP.getChipId();
  ip += "&version=";
  ip += VERSION_STRING;
  ip += "&type=VLPREC";
  
  int httpCode = http.POST(ip);
  if(httpCode > 0) {
    if(httpCode == HTTP_CODE_OK) {
      String payload = http.getString();
      if (payload == "ack") returnVal = true;
    }
  }
  http.end();
  return returnVal;
}

bool getPcIp(void)
{
  bool returnVal = false;
  HTTPClient http;
  http.begin("http://vlp.natfaulk.com/get_pc_ip");
  
  int httpCode = http.GET();
  if(httpCode > 0) {
    if(httpCode == HTTP_CODE_OK) {
      String payload = http.getString();
      if (remoteIP.fromString(payload))
      {
        returnVal = true;
        validRemoteIp = true;
      }
    }
  }
  http.end();
  return returnVal;
}

// called if ws doesnt start. ie doesnt connect to server
void getStartWsHandler(void) {
  startWs();
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  server.send(200, "text/plain", "ack");
#if SERVER_DEBUG_ENABLE
  DBG_pair("Remote IP", remoteIP.toString());
  DBG_pair("Websocket start request from", server.client().remoteIP().toString().c_str());
#endif
}

void startWs(void) {
  if (validRemoteIp)
  {
    webSocket.begin(remoteIP.toString(), WS_PORT, WS_PATH);
    webSocket.onEvent(webSocketEvent);
    webSocket.setReconnectInterval(5000);
  }
}

void SERVER_Tick(void)
{
  server.handleClient();
  handleUDPServer();

  if(validRemoteIp) webSocket.loop();
}

void rootHandler(void)
{
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  server.send(200, "text/plain", "Root handler, nothing implemented here yet...");
#if SERVER_DEBUG_ENABLE
  DBG_pair("Served / to", server.client().remoteIP().toString().c_str());
#endif
}

void notfoundHandler(void)
{
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  server.send(404, "text/plain", "404: Not found");
#if SERVER_DEBUG_ENABLE
  DBG_pair("Served 404 to", server.client().remoteIP().toString().c_str());
#endif
}

void getversionHandler(void)
{
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  server.send(200, "text/plain", VERSION_STRING);
#if SERVER_DEBUG_ENABLE
  String temp = "Sent version number to " + server.client().remoteIP().toString();
  DBG_s(temp.c_str());
#endif
}

void setremoteipHandler(void)
{
  String ip = server.arg("ip");
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  if (remoteIP.fromString(ip))
  {
    server.send(200, "text/plain", "ack");

    validRemoteIp = true;

    UDP.beginPacket(remoteIP, REMOTE_UDP_PORT);
    UDP.write("Info: got new remote IP");
    UDP.endPacket();
  }
  else server.send(200, "text/plain", "nack");
#if SERVER_DEBUG_ENABLE
  String temp = "Got new remote IP from:" + server.client().remoteIP().toString() + ", new remote ip is " + ip;
  DBG_s(temp.c_str());
#endif
}

void SERVER_SendUDP(const char* data)
{
  if (validRemoteIp)
  {
    UDP.beginPacket(remoteIP, REMOTE_UDP_PORT);
    UDP.write(data);
    UDP.endPacket();
  }
}

void SERVER_SendWS(const char* data)
{
  if (ws_connected) webSocket.sendTXT(data);
}

void handleUDPServer() {
  const int packetSize = 100;
  byte packetBuffer[packetSize];
  int bytesReceived = UDP.parsePacket();

  if (bytesReceived) {
    UDP.read(packetBuffer, packetSize);

    // String myData = "";
    // for(int i = 0; i < packetSize; i++) {
    //   myData += (char)packetBuffer[i];
    // }
    // DBG_pair("UDP data", myData);
  }
}

