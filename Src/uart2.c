#include "uart2.h"
#include <string.h>
#include <stdbool.h>

#define TX_BUF_LEN 1000
#define RX_BUF_LEN 1000

static UART_HandleTypeDef *huart;
static uint8_t txBuffer[TX_BUF_LEN];
static uint8_t rxBuffer[RX_BUF_LEN];

static volatile uint8_t *txHeadPtr = txBuffer;
static volatile uint8_t *txTailPtr = txBuffer;
static volatile uint8_t *rxHeadPtr = rxBuffer;
static volatile uint8_t *rxTailPtr = rxBuffer;

static bool rxBuffFull(void);
static uint16_t txBuffCharsUsed(void);


void UART2_Init(UART_HandleTypeDef *_huart)
{
  huart = _huart;
  __HAL_UART_ENABLE_IT(huart, UART_IT_RXNE);
}

void UART2_PutChar(char c)
{
  while(txBuffCharsUsed() > TX_BUF_LEN - 2);
  (* txHeadPtr) = c;
  ++txHeadPtr;
  if (txHeadPtr >= (txBuffer + TX_BUF_LEN)) txHeadPtr = txBuffer;
  __HAL_UART_ENABLE_IT(huart, UART_IT_TXE);
}

uint16_t UART2_CharsAvailable(void)
{
  if (rxHeadPtr >= rxTailPtr) return rxHeadPtr - rxTailPtr;
  else return RX_BUF_LEN - (rxTailPtr - rxHeadPtr);
}

uint8_t UART2_GetChar(void)
{
  // shouldn't read if no chars available. Fail gracefully and return a null char
  if (rxHeadPtr == rxTailPtr) return '\0';
  uint8_t retVal = (* rxTailPtr);
  rxTailPtr++;
  if (rxTailPtr >= (rxBuffer + RX_BUF_LEN)) rxTailPtr = rxBuffer;
  return retVal;
}

void UART2_SendStr(const char * str)
{
  uint16_t i = 0;
  while(str[i] != '\0') {
    UART2_PutChar(str[i++]);
  }
}

void UART2_SendUint32(uint32_t i)
{
  char tBuff[12];
  sprintf(tBuff, "%lu", i);
  UART2_SendStr(tBuff);
}

void UART2_SendDouble(double f)
{
  char tBuff[15];
  sprintf(tBuff, "%f", f);
  UART2_SendStr(tBuff);
}

bool rxBuffFull(void)
{
  if (rxTailPtr - rxHeadPtr == 1) return true;
  if ((rxHeadPtr - rxTailPtr) == RX_BUF_LEN - 1) return true;
  return false;
}

uint16_t txBuffCharsUsed(void)
{
  if (txHeadPtr >= txTailPtr) return txHeadPtr - txTailPtr;
  else return TX_BUF_LEN - (txTailPtr - txHeadPtr);
}

// void bufferPush(uint8_t *buffer, uint16_t bufferLen, uint8_t c)
// {
//
// }

// -----------------------------------------------------------------------------
// Interupts
// -----------------------------------------------------------------------------

void USART2_IRQHandler(void)
{
  if (USART2->SR & USART_SR_RXNE) {
    volatile uint32_t tmp;                  /* Must be volatile to prevent optimizations */
  	tmp = USART2->SR;                       /* Read status register */
  	tmp = USART2->DR;                       /* Read data register */
    // if receive buffer is full fail gracefully and overwrite oldest data
    if (rxBuffFull()) {
      rxTailPtr++;
      if (rxTailPtr >= (rxBuffer + RX_BUF_LEN)) rxTailPtr = rxBuffer;
    }
    (* rxHeadPtr) = tmp;
    ++rxHeadPtr;
    if (rxHeadPtr >= (rxBuffer + RX_BUF_LEN)) rxHeadPtr = rxBuffer;
  }

  if (USART2->SR & USART_SR_TXE) {
    // this shouldnt ever be true. Just in case
    if (txHeadPtr == txTailPtr) {
      __HAL_UART_DISABLE_IT(huart, UART_IT_TXE);
      return;
    }
    // send byte
    USART2->DR = (* txTailPtr);
    txTailPtr++;
    if (txTailPtr >= (txBuffer + TX_BUF_LEN)) txTailPtr = txBuffer;
    // if tx buffer empty disable intterupt
    if (!txBuffCharsUsed()) __HAL_UART_DISABLE_IT(huart, UART_IT_TXE);
  }
}
