
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"

/* USER CODE BEGIN Includes */
#include "uart.h"
#include "uart2.h" // horrible way of doing this. Too lazy to write a better way
#include <stdbool.h>
#include "arduinoFFT.h"
#include "imu.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim2;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
#define SAMPLE_RATE 20000
volatile uint16_t latestADC[NUM_SAMPLES] = {0};
volatile bool adcDone = false;
bool sendDataUart = false; // either send via uart or wifi
// if true over uart, if false over wifi

#define INPUT_BUFF_LEN 100
char inputBuff[INPUT_BUFF_LEN] = {0};
uint32_t i_inputBuff = 0;
char temp = '\0';

uint8_t i2cBuff[6];
uint8_t data[2];

uint32_t prevTime = 0;
// double vReal[NUM_SAMPLES];
// double vImag[NUM_SAMPLES];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_TIM2_Init(void);
static void MX_I2C1_Init(void);
static void MX_ADC1_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_USART1_UART_Init();
  MX_TIM2_Init();
  MX_I2C1_Init();
  MX_ADC1_Init();
  /* USER CODE BEGIN 2 */

  UART_Init(&huart1);
  UART2_Init(&huart2);
  UART_SendStr("Hello world\r\n");
  UART_SendStr("Version: ");
  UART_SendStr(VERSION_STR);
  UART_SendStr("\r\n");
  UART2_SendStr("Info:Hello world\n");
  HAL_TIM_Base_Start_IT(&htim2);
  HAL_ADC_Start(&hadc1);

  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, 1);

  HAL_I2C_Init(&hi2c1);
  HAL_Delay(1000);

  IMU_Init(&hi2c1);
  if (IMU_BeginAccel()) UART_SendStr("Accel setup OK\r\n");
  else UART_SendStr("Accel setup failed\r\n");
  if (IMU_BeginGyro()) UART_SendStr("Gyro setup OK\r\n");
  else UART_SendStr("Gyro setup failed\r\n");
  if (IMU_BeginMag(MAG_MODE_CONTINUOUS_8HZ)) UART_SendStr("Mag setup OK\r\n");
  else UART_SendStr("Mag setup failed\r\n");
  HAL_Delay(100);


  for (uint8_t i = 0; i < 128; i++)
  {
	  if (HAL_OK == HAL_I2C_IsDeviceReady(&hi2c1, i << 1, 1, 25))
	  {
		  UART_SendStr("Found device at: ");
		  UART_SendUint32(i);
		  UART_SendStr("\r\n");
	  }
  }

  HAL_Delay(100);

  // data[0] = 28;
  // data[1] = 0x18;
  // if (HAL_OK == HAL_I2C_Master_Transmit(&hi2c1, 0x68 << 1, data, 2, 50))
  // {
  //   UART_SendStr("Accel setup OK\r\n");
  // }
  // HAL_Delay(100);

  // data[0] = 27;
  // data[1] = 0x18;
  // if (HAL_OK == HAL_I2C_Master_Transmit(&hi2c1, 0x68 << 1, data, 2, 50))
  // {
  //   UART_SendStr("Gyro setup OK\r\n");
  // }
  // HAL_Delay(100);

  // FFT_Init(vReal, vImag, NUM_SAMPLES, SAMPLE_RATE);
  //HAL_ADC_Start_IT(&hadc1);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
	  if (UART_CharsAvailable())
	  {
		  if (i_inputBuff >= INPUT_BUFF_LEN - 2) i_inputBuff = 0;
      else
      {
        temp = UART_GetChar();
        if (temp == '\n')
        {
          inputBuff[i_inputBuff] = '\0';
          // parseInput();
          if (inputBuff[0] == 'b') 
          {
            sendDataUart = true;
            HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, 0);
          }
          else if (inputBuff[0] == 's')
          {
            sendDataUart = false;
            HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, 1);
          }

          i_inputBuff = 0;
        }
        else inputBuff[i_inputBuff++] = temp;
      }
	  }

    if (HAL_GetTick() - prevTime > 1000/8)
    {
      prevTime = HAL_GetTick();
      
      //HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_1);
      // HAL_Delay(1000);

      // data[0] = 0x3B;
      // HAL_I2C_Master_Transmit(&hi2c1, 0x68 << 1, data, 1, 50);
      // if (HAL_OK == HAL_I2C_Master_Receive(&hi2c1, 0x68 << 1, i2cBuff, 6, 50))
      // {
      //   UART_SendStr("Accel Data:\r\n");
    	//   for (uint8_t i = 0; i < 6; i+=2)
    	//   {
      //     int16_t resInt = - (i2cBuff[i] << 8 | i2cBuff[i + 1]);
      //     float resFloat = (float)resInt;
      //     resFloat *= (float)16;
      //     resFloat /= (float)0x8000;
    	// 	  UART_SendDouble(resFloat);
    	// 	  UART_SendStr("\r\n");
    	//   }
      // }

      // data[0] = 0x43;
      // HAL_I2C_Master_Transmit(&hi2c1, 0x68 << 1, data, 1, 50);
      // if (HAL_OK == HAL_I2C_Master_Receive(&hi2c1, 0x68 << 1, i2cBuff, 6, 50))
      // {
      //   UART_SendStr("Gyro Data:\r\n");
    	//   for (uint8_t i = 0; i < 6; i+=2)
    	//   {
      //     int16_t resInt = - (i2cBuff[i] << 8 | i2cBuff[i + 1]);
      //     float resFloat = (float)resInt;
      //     resFloat *= (float)16;
      //     resFloat /= (float)0x8000;
    	// 	  UART_SendDouble(resFloat);
    	// 	  UART_SendStr("\r\n");
    	//   }
      // }


      // for (int i = 0; i < NUM_SAMPLES; i++)
      // {
      //   vReal[i] = latestADC[i];
      //   vImag[i] = 0;
      // }


      // FFT_Windowing(FFT_WIN_TYP_HAMMING, FFT_FORWARD);  /* Weigh data */
      // FFT_Compute(FFT_FORWARD); /* Compute FFT */
      // FFT_ComplexToMagnitude(); /* Compute magnitudes */

  //	  while(!adcDone);
      if (sendDataUart)
      {
        UART_SendStr("[DEBUG] ADC Data:[");
        for (uint32_t i = 0; i < NUM_SAMPLES; i++)
        {
          UART_SendUint32(latestADC[i]);
          if (i < NUM_SAMPLES - 1) UART_PutChar(',');
        }
        UART_SendStr("]\r\n");
        adcDone = false;

        if (IMU_UpdateAccel() && IMU_UpdateGyro() && IMU_UpdateMag())
        {
          UART_SendStr("[DEBUG] IMU Data:[");
          UART_SendDouble(IMU_GetAccel()->x); UART_PutChar(',');
          UART_SendDouble(IMU_GetAccel()->y); UART_PutChar(',');
          UART_SendDouble(IMU_GetAccel()->z); UART_PutChar(',');
          UART_SendDouble(IMU_GetGyro()->x); UART_PutChar(',');
          UART_SendDouble(IMU_GetGyro()->y); UART_PutChar(',');
          UART_SendDouble(IMU_GetGyro()->z); UART_PutChar(',');
          UART_SendDouble(IMU_GetMag()->x); UART_PutChar(',');
          UART_SendDouble(IMU_GetMag()->y); UART_PutChar(',');
          UART_SendDouble(IMU_GetMag()->z);
          UART_SendStr("]\r\n");
        }
      }
      else
      {
        // send over wifi
        UART2_SendStr("ADC Data:[");
        for (uint32_t i = 0; i < NUM_SAMPLES; i++)
        {
          UART2_SendUint32(latestADC[i]);
          if (i < NUM_SAMPLES - 1) UART2_PutChar(',');
        }
        UART2_SendStr("]\n");
        adcDone = false;

        // if (IMU_UpdateAccel() && IMU_UpdateGyro() && IMU_UpdateMag())
        // {
        //   UART2_SendStr("IMU Data:[");
        //   UART2_SendDouble(IMU_GetAccel()->x); UART2_PutChar(',');
        //   UART2_SendDouble(IMU_GetAccel()->y); UART2_PutChar(',');
        //   UART2_SendDouble(IMU_GetAccel()->z); UART2_PutChar(',');
        //   UART2_SendDouble(IMU_GetGyro()->x); UART2_PutChar(',');
        //   UART2_SendDouble(IMU_GetGyro()->y); UART2_PutChar(',');
        //   UART2_SendDouble(IMU_GetGyro()->z); UART2_PutChar(',');
        //   UART2_SendDouble(IMU_GetMag()->x); UART2_PutChar(',');
        //   UART2_SendDouble(IMU_GetMag()->y); UART2_PutChar(',');
        //   UART2_SendDouble(IMU_GetMag()->z);
        //   UART2_SendStr("]\r\n");
        // }
      }
    }

	  //HAL_ADC_Start(&hadc1);

//	  for (uint32_t i = 0; i < NUM_SAMPLES; i++)
//	  {
//		  latestADC[i] = HAL_ADC_GetValue(&hadc1);
//
//	  }
//
//	  UART_SendUint32(HAL_ADC_GetValue(&hadc1));
//	  UART_SendStr("\r\n");
  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* ADC1 init function */
static void MX_ADC1_Init(void)
{

  ADC_ChannelConfTypeDef sConfig;

    /**Common config 
    */
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_4;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* I2C1 init function */
static void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM2 init function */
static void MX_TIM2_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 1439;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART1 init function */
static void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 460800;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 460800;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1|GPIO_PIN_8, GPIO_PIN_RESET);

  /*Configure GPIO pins : PB1 PB8 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
//void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
//{
//  if (hadc->Instance==ADC1)
//  {
//	//latestADC = HAL_ADC_GetValue(hadc);
//    // interruptCount++;
//  }
//}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
