#include "imu.h"
#include <string.h> // memcpy()

#define TIMEOUT_MS            50

#define GYRO_CONFIG           0x1B
#define ACCEL_CONFIG          0x1C
#define ACCEL_DATA            0x3B
#define GYRO_DATA             0x43

#define AK8963_ADDRESS  0x0C
#define AK8963_RA_HXL   0x03
#define AK8963_RA_CNTL1 0x0A
#define AK8963_RA_ASAX  0x10

#define MPU9250_RA_WHOAMI 0x75

bool magSetMode(uint8_t mode);

uint8_t dataOut[2];
uint8_t accelBuff[6];
uint8_t gyroBuff[6];
uint8_t magBuff[7];
uint8_t address;
float accelRange, gyroRange;

uint8_t magAdjust[3] = {0};
int16_t magOffset[3] = {0};

ImuData accelData;
ImuData gyroData;
ImuData magData;

I2C_HandleTypeDef *hi2c;

void IMU_Init(I2C_HandleTypeDef *_hi2c)
{
  hi2c = _hi2c;
  address = MPU9250_ADDRESS_AD0_LOW;
}

bool IMU_BeginAccel(void)
{
  dataOut[0] = ACCEL_CONFIG;
  dataOut[1] = ACC_FULL_SCALE_16_G;
  accelRange = 16.0;
  return (HAL_OK == HAL_I2C_Master_Transmit(hi2c, address << 1, dataOut, 2, TIMEOUT_MS));
}

bool IMU_BeginGyro(void)
{
  dataOut[0] = GYRO_CONFIG;
  dataOut[1] = GYRO_FULL_SCALE_2000_DPS;
  gyroRange = 2000.0;
  return (HAL_OK == HAL_I2C_Master_Transmit(hi2c, address << 1, dataOut, 2, TIMEOUT_MS));
}

bool IMU_BeginMag(uint8_t mode)
{
  // Trun on AK8963 magnetometer
  dataOut[0] = 0x37;
  dataOut[1] = 0x02;
  if (HAL_OK != HAL_I2C_Master_Transmit(hi2c, address << 1, dataOut, 2, TIMEOUT_MS)) return false;
  HAL_Delay(10);

  // magReadAdjustValues
  if (!magSetMode(MAG_MODE_POWERDOWN)) return false;
  if (!magSetMode(MAG_MODE_FUSEROM)) return false;
  dataOut[0] = AK8963_RA_ASAX;
  if (HAL_OK != HAL_I2C_Master_Transmit(hi2c, AK8963_ADDRESS << 1, dataOut, 1, TIMEOUT_MS)) return false;
  uint8_t buff[3];
  if (HAL_OK != HAL_I2C_Master_Receive(hi2c, AK8963_ADDRESS << 1, buff, 3, TIMEOUT_MS)) return false;
  memcpy(magAdjust, buff, 3);
  HAL_Delay(10);

  if (!magSetMode(MAG_MODE_POWERDOWN)) return false;
  if (!magSetMode(mode)) return false;
  HAL_Delay(10);

  return true;
}

bool magSetMode(uint8_t mode)
{
  dataOut[0] = AK8963_RA_CNTL1;
  dataOut[1] = mode;
  if (HAL_OK != HAL_I2C_Master_Transmit(hi2c, AK8963_ADDRESS << 1, dataOut, 2, TIMEOUT_MS)) return false;
  HAL_Delay(10);
  return true;
}

bool IMU_UpdateAccel(void)
{
  dataOut[0] = ACCEL_DATA;
  if (HAL_OK != HAL_I2C_Master_Transmit(hi2c, address << 1, dataOut, 1, TIMEOUT_MS)) return false;
  if (HAL_OK != HAL_I2C_Master_Receive(hi2c, address << 1, accelBuff, 6, TIMEOUT_MS)) return false;
  
  for (uint8_t i = 0; i < 6; i+=2)
  {
    int16_t resInt = -((uint16_t)accelBuff[i] << 8 | accelBuff[i + 1]);
    float resFloat = (float)resInt;
    resFloat *= accelRange;
    resFloat /= (float)0x8000;
    ((float *)&accelData)[i / 2] = resFloat;
  }

  return true;
}

bool IMU_UpdateGyro(void)
{
  dataOut[0] = GYRO_DATA;
  if (HAL_OK != HAL_I2C_Master_Transmit(hi2c, address << 1, dataOut, 1, TIMEOUT_MS)) return false;
  if (HAL_OK != HAL_I2C_Master_Receive(hi2c, address << 1, gyroBuff, 6, TIMEOUT_MS)) return false;

  for (uint8_t i = 0; i < 6; i+=2)
  {
    int16_t resInt = -((uint16_t)gyroBuff[i] << 8 | gyroBuff[i + 1]);
    float resFloat = (float)resInt;
    resFloat *= gyroRange;
    resFloat /= (float)0x8000;
    ((float *)&gyroData)[i / 2] = resFloat;
  }

  return true;
}

bool IMU_UpdateMag(void)
{
  dataOut[0] = AK8963_RA_HXL;
  if (HAL_OK != HAL_I2C_Master_Transmit(hi2c, AK8963_ADDRESS << 1, dataOut, 1, TIMEOUT_MS)) return false;
  if (HAL_OK != HAL_I2C_Master_Receive(hi2c, AK8963_ADDRESS << 1, magBuff, 7, TIMEOUT_MS)) return false;

  for (uint8_t i = 0; i < 6; i+=2)
  {
    int16_t resInt = (uint16_t)magBuff[i + 1] << 8 | magBuff[i];
    float resFloat = ((float) resInt * (((((float) magAdjust[i / 2] - 128) * 0.5) / 128) + 1));
    resFloat += magOffset[i/2];
    ((float *)&magData)[i / 2] = resFloat;
  }

  return true;
}


ImuData* IMU_GetAccel(void)
{
  return &accelData;
}

ImuData* IMU_GetGyro(void)
{
  return &gyroData;
}

ImuData* IMU_GetMag(void)
{
  return &magData;
}


